package com.github.kittinunf.fuel.reactor;

import com.github.kittinunf.fuel.test.MockHelper;

import org.junit.After;
import org.junit.Before;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * BaseTestCase
 *
 * @since 2021-05-29
 */
public abstract class BaseTestCase {
    private static final long DEFAULT_TIMEOUT = 15L;
    /**
     * MockHelper
     */
    public MockHelper mock;
    /**
     * CountDownLatch
     */
    public CountDownLatch lock;

    protected MockHelper getMock() {
        return this.mock;
    }

    protected void setMock(MockHelper mockHelper) {
        this.mock = mockHelper;
    }

    /**
     * getLock
     *
     * @return CountDownLatch
     */
    public CountDownLatch getLock() {
        return this.lock;
    }

    /**
     * setLock
     *
     * @param latch CountDownLatch
     */
    public void setLock(CountDownLatch latch) {
        this.lock = latch;
    }

    /**
     * await
     */
    public void await() {
        try {
            this.lock.await(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * setup
     */
    @Before
    public final void setup() {
        this.mock = new MockHelper();
        this.lock = new CountDownLatch(1);
        this.mock.setup();
    }

    /**
     * breakdown
     */
    @After
    public void breakdown() {
        this.mock.tearDown();
    }
}
