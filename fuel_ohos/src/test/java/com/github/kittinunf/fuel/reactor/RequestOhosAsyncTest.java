/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.kittinunf.fuel.reactor;


import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonNodeReader;
import com.github.kittinunf.fuel.Fuel;
import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.fuel.core.FuelManager;
import com.github.kittinunf.fuel.core.Request;
import com.github.kittinunf.fuel.core.Response;
import com.github.kittinunf.fuel.core.ResponseDeserializable;
import com.github.kittinunf.fuel.core.ResponseHandler;
import com.github.kittinunf.result.Result;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.HttpTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.text.Charsets;

/**
 * @since 2021-05-26
 */
public class RequestOhosAsyncTest extends BaseTestCase {
    /**
     * setupFuelManager
     */
    @Before
    public void setupFuelManager() {
        FuelManager fuelManager = FuelManager.Companion.getInstance();
        Map map = new HashMap<String, String>();
        map.put("foo", "bar");
        fuelManager.setBaseHeaders(map);
        List<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair<>("key", "value"));
        fuelManager.setBaseParams(list);
    }

    /**
     * resetFuelManager
     */
    @After
    public void resetFuelManager() {
        FuelManager.Companion.getInstance().reset();
    }

    /**
     * setUp
     */
    @Before
    public void setUp() {
        this.setLock(new CountDownLatch(1));
    }

    /**
     * HttpBinHeadersModel
     */
    public static class HttpBinHeadersModel {
        /**
         * headers
         */
        public Map<String, List<String>> headers;

        public HttpBinHeadersModel() {
            headers = new HashMap<>();
        }

        public Map<String, List<String>> getHeaders() {
            return headers;
        }

        public void setHeaders(Map<String, List<String>> headers) {
            this.headers = headers;
        }
    }

    /**
     * HttpBinHeadersDeserializer
     */
    public static class HttpBinHeadersDeserializer implements ResponseDeserializable<HttpBinHeadersModel> {
        /**
         * HttpBinHeadersDeserializer
         */
        public HttpBinHeadersDeserializer() {
        }

        @NotNull
        @Override
        public HttpBinHeadersModel deserialize(@NotNull Response response) {
            HttpBinHeadersModel httpBinHeadersModel = new HttpBinHeadersModel();

            JsonNodeReader jsonNodeReader = new JsonNodeReader();
            InputStream stream = response.getBody$fuel().toStream();
            Reader reader = new InputStreamReader(stream, Charsets.UTF_8);
            try {
                JsonNode onde = jsonNodeReader.fromReader(reader);
                Iterator<String> keys = onde.fieldNames();
                Iterator<JsonNode> readElements = onde.elements();

                httpBinHeadersModel.setHeaders(deserializeHeaders(readElements, keys));

            } catch (IOException exception) {
                exception.printStackTrace();
            } finally {
                try {
                    stream.close();
                    reader.close();
                } catch (IOException eX) {
                    eX.printStackTrace();
                }
            }
            return httpBinHeadersModel;
        }

        @Nullable
        @Override
        public HttpBinHeadersModel deserialize(@NotNull InputStream inputStream) {
            return null;
        }

        @Nullable
        @Override
        public HttpBinHeadersModel deserialize(@NotNull Reader reader) {
            return null;
        }

        @Nullable
        @Override
        public HttpBinHeadersModel deserialize(@NotNull byte[] bytes) {
            return null;
        }

        @Nullable
        @Override
        public HttpBinHeadersModel deserialize(@NotNull String str) {
            return null;
        }

        private Map<String, List<String>> deserializeHeaders(Iterator<JsonNode> reader, Iterator<String> keys) {
            Map result = new HashMap<String, JsonNode>();

            while (reader.hasNext()) {
                while (keys.hasNext()) {
                    String name = keys.next();
                    JsonNode value = reader.next();
                    result.put(name, value);
                }
            }

            JsonNode headersNode = (JsonNode) result.get("headers");
            result.clear();

            result = new HashMap<String, List<String>>();
            Iterator<JsonNode> iterator = headersNode.elements();
            Iterator<String> keyIterator = headersNode.fieldNames();
            while (iterator.hasNext()) {
                while (keyIterator.hasNext()) {
                    String key = keyIterator.next();
                    List list = new ArrayList<>();
                    JsonNode headersValue = iterator.next();
                    list.add(headersValue);
                    result.put(key, list);
                }
            }

            return result;
        }
    }

    /**
     * httpGetRequestString
     */
    @Test
    public void httpGetRequestString() {
        final Request[] request = {null};
        final Response[] response = {null};
        final List data = new ArrayList();
        final FuelError[] error = new FuelError[1];

        HttpRequest httpRequest = mock.request().withPath("/user-agent");
        HttpTemplate httpTemplate = mock.reflect();
        mock.chain(httpRequest, httpTemplate);

        String path = "";
        try {
            path = mock.path("user-agent");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (path == null || path.length() <= 0) {
            return;
        }
        Fuel.INSTANCE.get(path, null).responseString(new Function3<Request, Response,
            Result<String, ? extends FuelError>, Unit>() {
            @Override
            public Unit invoke(Request req, Response resp, Result<String, ? extends FuelError> stringResult) {
                data.add(stringResult.component1());
                error[0] = stringResult.component2();
                request[0] = req;
                response[0] = resp;
                lock.countDown();
                Optional<Unit> optionalUnit = Optional.empty();
                return optionalUnit.get();
            }
        });

        await();

        Assert.assertThat(request[0], CoreMatchers.notNullValue());
        Assert.assertThat(response[0], CoreMatchers.notNullValue());
        Assert.assertThat(error[0], CoreMatchers.nullValue());
        Assert.assertThat(data.get(0), CoreMatchers.notNullValue());
        Assert.assertThat(data.get(0), (Matcher) CoreMatchers.isA(String.class));

        Assert.assertThat(response[0] != null ? response[0].getStatusCode() : null,
            CoreMatchers.is(Integer.valueOf(HttpURLConnection.HTTP_OK)));
    }

    /**
     * httpGetRequestJsonValid
     */
    @Test
    public void httpGetRequestJsonValid() {
        final Request[] request = {null};
        final Response[] response = {null};
        final List data = new ArrayList();
        final FuelError[] error = new FuelError[1];

        HttpRequest httpRequest = mock.request().withPath("/user-agent");
        HttpTemplate httpTemplate = mock.reflect();
        mock.chain(httpRequest, httpTemplate);

        String path = "";
        try {
            path = mock.path("user-agent");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (path == null || path.length() <= 0) {
            return;
        }

        Fuel.INSTANCE.get(path, null).responseString(new Function3<Request, Response,
            Result<String, ? extends FuelError>, Unit>() {
            @Override
            public Unit invoke(Request req, Response resp, Result<String, ? extends FuelError> stringResult) {
                data.add(stringResult.component1());
                error[0] = stringResult.component2();
                request[0] = req;
                response[0] = resp;
                lock.countDown();
                Optional<Unit> optionalUnit = Optional.empty();
                return optionalUnit.get();
            }
        });

        await();

        Assert.assertThat(request[0], CoreMatchers.notNullValue());
        Assert.assertThat(response[0], CoreMatchers.notNullValue());
        Assert.assertThat(error[0], CoreMatchers.nullValue());
        Assert.assertThat(data.get(0), CoreMatchers.notNullValue());

        Assert.assertThat(data.get(0), (Matcher) CoreMatchers.isA(String.class));

        Assert.assertThat(response[0] != null ? response[0].getStatusCode() : null,
            CoreMatchers.is(Integer.valueOf(HttpURLConnection.HTTP_OK)));
    }

    /**
     * httpGetRequestJsonHandlerValid
     */
    @Test
    public void httpGetRequestJsonHandlerValid() {
        final Request[] req = {null};
        final Response[] resp = {null};
        final List data = new ArrayList();
        final FuelError[] error = new FuelError[1];

        HttpRequest httpRequest = mock.request().withPath("/user-agent");
        HttpTemplate httpTemplate = mock.reflect();
        mock.chain(httpRequest, httpTemplate);

        String path = "";
        try {
            path = mock.path("user-agent");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (path == null || path.length() <= 0) {
            return;
        }

        Fuel.INSTANCE.get(path, null).responseString(new ResponseHandler<String>() {
            @Override
            public void success(@NotNull Request request, @NotNull Response response, String str) {
                data.add(str);
                req[0] = request;
                resp[0] = response;

                lock.countDown();
            }

            @Override
            public void failure(@NotNull Request request, @NotNull Response response, @NotNull FuelError fuelError) {
                error[0] = fuelError;

                lock.countDown();
            }
        });

        await();

        Assert.assertThat(req[0], CoreMatchers.notNullValue());
        Assert.assertThat(resp[0], CoreMatchers.notNullValue());
        Assert.assertThat(error[0], CoreMatchers.nullValue());
        Assert.assertThat(data.get(0), CoreMatchers.notNullValue());

        Assert.assertThat(data.get(0), (Matcher) CoreMatchers.isA(String.class));

        Assert.assertThat(resp[0] != null ? resp[0].getStatusCode() : null,
            CoreMatchers.is(Integer.valueOf(HttpURLConnection.HTTP_OK)));
    }

    /**
     * httpGetRequestJsonInvalid
     */
    @Test
    public void httpGetRequestJsonInvalid() {
        final Request[] request = {null};
        final Response[] response = {null};
        final List data = new ArrayList();
        final FuelError[] error = new FuelError[1];

        HttpRequest httpRequest = mock.request().withPath("/404");
        HttpResponse httpResponse = mock.response().withStatusCode(HttpURLConnection.HTTP_NOT_FOUND);
        mock.chain(httpRequest, httpResponse);

        String path = "";
        try {
            path = mock.path("404");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (path == null || path.length() <= 0) {
            return;
        }

        Fuel.INSTANCE.get(path, null).responseString(new Function3<Request, Response,
            Result<String, ? extends FuelError>, Unit>() {
            @Override
            public Unit invoke(Request req, Response resp, Result<String, ? extends FuelError> stringResult) {
                data.add(stringResult.component1());
                error[0] = stringResult.component2();
                request[0] = req;
                response[0] = resp;
                lock.countDown();
                Optional<Unit> optionalUnit = Optional.empty();
                return optionalUnit.get();
            }
        });

        await();

        Assert.assertThat(request[0], CoreMatchers.notNullValue());
        Assert.assertThat(response[0], CoreMatchers.notNullValue());
        Assert.assertThat(error[0], CoreMatchers.notNullValue());
        Assert.assertThat(data.get(0), CoreMatchers.nullValue());

        Assert.assertThat(response[0] != null ? response[0].getStatusCode() : null,
            CoreMatchers.is(Integer.valueOf(HttpURLConnection.HTTP_NOT_FOUND)));
    }

    /**
     * httpGetRequestJsonHandlerInvalid
     */
    @Test
    public void httpGetRequestJsonHandlerInvalid() {
        final Request[] req = {null};
        final Response[] resp = {null};
        final List data = new ArrayList();
        final FuelError[] error = new FuelError[1];

        HttpRequest httpRequest = mock.request().withPath("/404");
        HttpResponse httpResponse = mock.response().withStatusCode(HttpURLConnection.HTTP_NOT_FOUND);
        mock.chain(httpRequest, httpResponse);

        String path = "";
        try {
            path = mock.path("404");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (path == null || path.length() <= 0) {
            return;
        }

        Fuel.INSTANCE.get(path, null).responseString(new ResponseHandler<String>() {
            @Override
            public void success(@NotNull Request request, @NotNull Response response, String str) {
                data.add(str);
                lock.countDown();
            }

            @Override
            public void failure(@NotNull Request request, @NotNull Response response, @NotNull FuelError fuelError) {
                req[0] = request;
                resp[0] = response;
                error[0] = fuelError;

                lock.countDown();
            }
        });

        await();

        Assert.assertThat(req[0], CoreMatchers.notNullValue());
        Assert.assertThat(resp[0], CoreMatchers.notNullValue());
        Assert.assertThat(error[0], CoreMatchers.notNullValue());
        Assert.assertThat(data.get(0), CoreMatchers.nullValue());

        Assert.assertThat(resp[0] != null ? resp[0].getStatusCode() : null,
            CoreMatchers.is(Integer.valueOf(HttpURLConnection.HTTP_NOT_FOUND)));
    }

    /**
     * httpGetRequestObject
     */
    @Test
    public void httpGetRequestObject() {
        final Request[] req = {null};
        final Response[] resp = {null};
        final HttpBinHeadersModel[] data = new HttpBinHeadersModel[1];
        final FuelError[] error = new FuelError[1];

        HttpRequest httpRequest = mock.request().withPath("/headers");
        HttpTemplate httpTemplate = mock.reflect();
        mock.chain(httpRequest, httpTemplate);

        String path = "";
        try {
            path = mock.path("headers");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (path == null || path.length() <= 0) {
            return;
        }

        Fuel.INSTANCE.get(path, null).responseObject(new HttpBinHeadersDeserializer(),
            new Function3<Request, Response, Result<? extends HttpBinHeadersModel, ? extends FuelError>, Unit>() {
            @Override
            public Unit invoke(Request request, Response response,Result<?
                extends HttpBinHeadersModel, ? extends FuelError> result) {
                data[0] = result.component1();
                error[0] = result.component2();
                req[0] = request;
                resp[0] = response;
                lock.countDown();
                Optional<Unit> optionalUnit = Optional.empty();
                return optionalUnit.get();
            }
        });

        await();

        Assert.assertThat(req[0], CoreMatchers.notNullValue());
        Assert.assertThat(resp[0], CoreMatchers.notNullValue());
        Assert.assertThat(error[0], CoreMatchers.nullValue());
        Assert.assertThat(data[0], CoreMatchers.notNullValue());
        Assert.assertThat(!data[0].headers.isEmpty(), CoreMatchers.is(true));
        List list = data[0].headers.get("foo");

        String value = list.get(0).toString().substring(2, 5);
        Assert.assertThat(value, CoreMatchers.is("bar"));
        int statusCode = HttpURLConnection.HTTP_OK;
        Assert.assertThat(resp[0] != null ? resp[0].getStatusCode() : null, CoreMatchers.is(statusCode));
    }

    /**
     * httpGetRequestHandlerObject
     */
    @Test
    public void httpGetRequestHandlerObject() {
        final Request[] req = {null};
        final Response[] resp = {null};
        final HttpBinHeadersModel[] data = new HttpBinHeadersModel[1];
        final FuelError[] error = new FuelError[1];

        HttpRequest httpRequest = mock.request().withPath("/headers");
        HttpTemplate httpTemplate = mock.reflect();
        mock.chain(httpRequest, httpTemplate);

        String path = "";
        try {
            path = mock.path("headers");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (path == null || path.length() <= 0) {
            return;
        }

        Fuel.INSTANCE.get(path, null).responseObject(new HttpBinHeadersDeserializer(),
            new ResponseHandler<HttpBinHeadersModel>() {
            @Override
            public void success(@NotNull Request request, @NotNull
                Response response, HttpBinHeadersModel httpBinHeadersModel) {
                data[0] = httpBinHeadersModel;
                req[0] = request;
                resp[0] = response;

                lock.countDown();
            }

            @Override
            public void failure(@NotNull Request request, @NotNull Response response, @NotNull FuelError fuelError) {
                error[0] = fuelError;

                lock.countDown();
            }
        });
        await();

        Assert.assertThat(req[0], CoreMatchers.notNullValue());
        Assert.assertThat(resp[0], CoreMatchers.notNullValue());
        Assert.assertThat(error[0], CoreMatchers.nullValue());
        Assert.assertThat(data[0], CoreMatchers.notNullValue());
        Assert.assertThat(!data[0].headers.isEmpty(), CoreMatchers.is(true));

        List list = data[0].headers.get("foo");

        String value = list.get(0).toString().substring(2, 5);
        Assert.assertThat(value, CoreMatchers.is("bar"));
        int statusCode = HttpURLConnection.HTTP_OK;
        Assert.assertThat(resp[0] != null ? resp[0].getStatusCode() : null, CoreMatchers.is(statusCode));
    }
}
