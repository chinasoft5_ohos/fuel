/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.kittinunf.ohos.util;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import com.github.kittinunf.fuel.core.Environment;

import java.util.concurrent.Executor;


/**
 * @since 2021-06-07
 */
public class OhosEnvironment implements Environment {
    private EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
    private Executor callbackExecutor = (Executor) (new Executor() {
        /**
         * execute
         * @param command Runnable
         */
        public void execute(Runnable command) {
            getHandler().postTask(command);
        }
    });

    /**
     * getHandler
     *
     * @return EventHandler
     */
    public EventHandler getHandler() {
        return this.handler;
    }

    /**
     * getCallbackExecutor
     *
     * @return Executor
     */
    public Executor getCallbackExecutor() {
        return this.callbackExecutor;
    }

    /**
     * setCallbackExecutor
     *
     * @param executor executor
     */
    public void setCallbackExecutor(Executor executor) {
        this.callbackExecutor = executor;
    }
}
