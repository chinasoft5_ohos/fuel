# fuel
#### 项目介绍
- 项目名称：fuel
- 所属系列：openharmony的第三方组件适配移植
- 功能：最简单的 HTTP 网络库
- 项目移植状态：主功能完成
- 调用差异：有(缺少LiveData和协程实现)
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 2.3.1

#### 效果演示
![avatar](screenshot/fuel.gif)

#### 安装教程
1.在项目根目录下的build.gradle文件中,
 ```
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'
    }
}
 ```

2.在entry模块的build.gradle文件中，
 ```
dependencies {
    implementation "org.jetbrains.kotlin:kotlin-stdlib:1.4.10"
    implementation "com.github.kittinunf.result:result:3.1.0"
    implementation "io.reactivex.rxjava2:rxjava:2.2.19"
    
    implementation 'com.gitee.chinasoft_ohos:fuel-ohos:1.0.0'
    implementation 'com.gitee.chinasoft_ohos:fuel-stetho:1.0.0'
    implementation 'com.github.kittinunf.fuel:fuel-gson:2.3.1'
    implementation 'com.github.kittinunf.fuel:fuel-rxjava:2.3.1'
    implementation 'com.github.kittinunf.fuel:fuel-forge:2.3.1'
    implementation 'com.github.kittinunf.fuel:fuel-jackson:2.3.1'
    implementation 'com.github.kittinunf.fuel:fuel-json:2.3.1'
    implementation 'com.github.kittinunf.fuel:fuel-kotlinx-serialization:2.3.1'
    implementation 'com.github.kittinunf.fuel:fuel-moshi:2.3.1'
    implementation 'com.github.kittinunf.fuel:fuel-reactor:2.3.1'    
}
 ```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
初始化范例
```java
  FuelManager.Companion.getInstance().setBasePath("http://httpbin.org");
  Map map = new HashMap();
  map.put("Device", "ohos");
  FuelManager.Companion.getInstance().setBaseHeaders(map);
  
  List<Pair<String, String>> list = new ArrayList<>();
  
  list.add(new Pair<>("key", "value"));
  FuelManager.Companion.getInstance().setBaseParams(list);
  
  FuelManager.Companion.getInstance().setHook(new StethoHook("Fuel Sample App"));
```
GET

```java
  FuelKt.httpGet("/get", null).responseString(new Function3<Request, Response, Result<String, ? extends FuelError>, Unit>() {
      @Override
      public Unit invoke(Request request, Response response, Result<String, ? extends FuelError> stringResult) {
          return null;
      }
  });
```

PUT
```java
  FuelKt.httpPut("/put", getPairList()).responseString(new Function3<Request, Response, Result<String, ? extends FuelError>, Unit>() {
      @Override
      public Unit invoke(Request request, Response response, Result<String, ? extends FuelError> stringResult) {
          return null;
      }
  });
```

POST
```java
  FuelKt.httpPost("/post", getPairList()).responseString(new Function3<Request, Response, Result<String, ? extends FuelError>, Unit>() {
      @Override
      public Unit invoke(Request request, Response response, Result<String, ? extends FuelError> stringResult) {                  return null;
      }
  });
```

PATCH
```java
  FuelManager.Companion.getInstance().setBasePath("http://httpbin.org");
  Request request = FuelManager.Companion.getInstance().request(Method.PATCH, "/patch", getPairList());
  request.responseString(new Function3<Request, Response, Result<String, ? extends FuelError>, Unit>() {
      @Override
      public Unit invoke(Request request, Response response, Result<String, ? extends FuelError> stringResult) {
          return null;
      }
  });
```

DELETE
```java
  FuelKt.httpDelete("/delete", getPairList()).responseString(new Function3<Request, Response, Result<String, ? extends FuelError>, Unit>() {
      @Override
      public Unit invoke(Request request, Response response, Result<String, ? extends FuelError> stringResult) {
          return null;
      }
  });
```

DOWNLOAD
```java
  DownloadRequest downloadRequest = Fuel.INSTANCE.download("/bytes/" + 1024 * 100, Method.GET, null);
  downloadRequest.fileDestination(new Function2<Response, Request, File>() {
      @Override
      public File invoke(Response response, Request request) {
          File file = new File(FileUtils.getSavePath(getContext(), "test.tmp"));
          return file;
      }
  });
  downloadRequest.progress(new Function2<Long, Long, Unit>() {
      @Override
      public Unit invoke(Long aLong, Long aLong2) {
          return null;
      }
  });
  downloadRequest.responseString(new Function3<Request, Response, Result<String, ? extends FuelError>, Unit>() {
      @Override
      public Unit invoke(Request request, Response response, Result<String, ? extends FuelError> stringResult) {
          return null;
      }
  });
```

UPLOAD
```java
   UploadRequest uploadRequest = Fuel.INSTANCE.upload("/post", Method.POST, null);
   File file = new File(FileUtils.getSavePath(getContext(), "out.tmp"));
   file = writeFile(file);
  
   String name = getFileNameWithoutExtention(file.getName());
   String contentDisposition = "form-data; name=" + (file.getName() != null ? "; filename=" + file.getName() : "");
   FileDataPart fileDataPart = new FileDataPart(file, name, file.getName(),
   FileDataPart.Companion.guessContentType(file), contentDisposition);
   uploadRequest.add(fileDataPart);
   uploadRequest.responseString(new Function3<Request, Response, Result<String, ? extends FuelError>, Unit>() {
       @Override
       public Unit invoke(Request request, Response response, Result<String, ? extends FuelError> stringResult) {
           return null;
       }
   });
```


#### 测试信息
CodeCheck代码测试无异常  

CloudTest代码测试无异常  

病毒安全检测通过  

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.0

#### 版权和许可信息
Fuel is released under the MIT license.
