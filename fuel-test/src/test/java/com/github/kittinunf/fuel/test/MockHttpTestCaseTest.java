/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.kittinunf.fuel.test;

import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.fuel.core.FuelManager;
import com.github.kittinunf.fuel.core.Method;
import com.github.kittinunf.fuel.core.Request;
import com.github.kittinunf.fuel.core.Response;
import com.github.kittinunf.result.Result;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpTemplate;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;

/**
 * @since 2021-09-26
 */
public class MockHttpTestCaseTest {

    /**
     * mock
     */
    protected MockHelper mock;

    /**
     * setup
     */
    @Before
    public void setup() {
        // You can set the log level to INFO or TRACE to see all the mocking logging
        this.mock = new MockHelper();
        this.mock.setup();
    }

    @After
    public void tearDown() {
        this.mock.tearDown();
    }

    @Test
    public void reflectRequest() throws MalformedURLException{
        String BASE_PATH = "http://httpbin.org";

        FuelManager.Companion.getInstance().setBasePath(BASE_PATH);

        Map map = new HashMap();
        map.put("Device", "ohos");
        FuelManager.Companion.getInstance().setBaseHeaders(map);

        List<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair<>("key", "value"));
        FuelManager.Companion.getInstance().setBaseParams(list);
        FuelManager.Companion.getInstance().setForceMethods(true);


        HttpRequest req = mock.request().withMethod(Method.PATCH.getValue()).withPath("/patch");
        HttpTemplate response = mock.reflect();
        mock.chain(req, response);
        Request request = FuelManager.Companion.getInstance().request(Method.PATCH, mock.path("/patch"), list);
        request.responseString(new Function3<Request, Response, Result<String, ? extends FuelError>, Unit>() {
            @Override
            public Unit invoke(Request request, Response response, Result<String, ? extends FuelError> stringResult) {
                System.out.println(stringResult.toString());
                Optional<Unit> optionalUnit = Optional.empty();
                return optionalUnit.get();
            }
        });
    }

}
