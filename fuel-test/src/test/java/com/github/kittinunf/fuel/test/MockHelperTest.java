/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.kittinunf.fuel.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;

/**
 * @since 2021-09-26
 */
public class MockHelperTest {
    /**
     * mock
     */
    protected MockHelper mock;

    /**
     * setup
     */
    @Before
    public void setup() {
        // You can set the log level to INFO or TRACE to see all the mocking logging
        this.mock = new MockHelper();
        this.mock.setup();
    }

    @After
    public void tearDown() {
        this.mock.tearDown();
    }

    @Test
    public void path() throws MalformedURLException{
        String url = mock.path("");
        System.out.println(url);
    }

}
