/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.kittinunf.fuel.test;

import com.github.kittinunf.fuel.core.FuelManager;
import com.github.kittinunf.fuel.core.Method;
import com.github.kittinunf.fuel.core.Request;

import org.junit.After;
import org.junit.Before;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpTemplate;

import java.net.MalformedURLException;
import java.util.List;

import kotlin.Pair;


/**
 * @since 2021-05-27
 */
public class MockHttpTestCase {
    /**
     * mock
     */
    protected MockHelper mock;

    /**
     * setup
     */
    @Before
    public void setup() {
        // You can set the log level to INFO or TRACE to see all the mocking logging
        this.mock = new MockHelper();
        this.mock.setup();
    }

    /**
     * tearDown
     */
    @After
    public void tearDown() {
        this.mock.tearDown();
    }

    /**
     * reflectedRequest
     * @param method method
     * @param path path
     * @param parameters parameters
     * @param manager manager
     * @return Request
     * @throws MalformedURLException
     */
    public Request reflectRequest(Method method, String path, List<Pair<String, Object>>
        parameters, FuelManager manager)
        throws MalformedURLException {
        HttpRequest request = mock.request().withMethod(method.getValue()).withPath("/" + path);
        HttpTemplate response = mock.reflect();
        mock.chain(request, response);
        return manager.request(method, mock.path(path), parameters);
    }
}
