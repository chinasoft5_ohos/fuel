/*
 * Copyright (C) 2021 The Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.fuel;

import ohos.app.Context;
import ohos.app.Environment;
import ohos.data.usage.DataUsage;
import ohos.data.usage.MountState;

import java.io.File;

/**
 * @since 2021-04-19
 */
public class FileUtils {
    /**
     * 获取存储路径
     *
     * @param context context
     * @param fileName fileName
     * @return 返回存储路径
     */
    public static String getSavePath(Context context, String fileName) {
        return getDiskCacheDir(context) + File.separator + fileName;
    }

    /**
     * 获取磁盘的缓存目录
     *
     * @param context context
     * @return SD卡不存在: /data/data/com.xxx.xxx/cache;<br>
     */
    public static String getDiskCacheDir(Context context) {
        String path = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath();
        return context.getExternalCacheDir() != null ?  path: context.getCacheDir().getPath();
    }

    /**
     * SD卡是否存在
     *
     * @return boolean
     */
    public boolean isSDCardExist() {
        return DataUsage.getDiskMountedStatus().equals(MountState.DISK_MOUNTED) && DataUsage.isDiskPluggable();
    }
}
