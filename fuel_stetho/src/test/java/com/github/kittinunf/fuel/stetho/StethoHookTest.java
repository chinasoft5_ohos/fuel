package com.github.kittinunf.fuel.stetho;

import com.github.kittinunf.fuel.core.FuelManager;
import com.github.kittinunf.fuel.core.Request;
import com.github.kittinunf.fuel.test.MockHttpTestCase;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;

/**
 * StethoHookTest
 *
 * @since 2021-05-29
 */
public class StethoHookTest extends MockHttpTestCase {
    private StethoHook hook = new StethoHook("StethoFuelConnectionManager");

    /**
     * stethoConnectionIsCreated
     *
     * @throws IOException IOException
     */
    @Test
    public void stethoConnectionIsCreated() throws IOException {
        ConcurrentHashMap map = hook.getStethoCache();
        URL url = new URL("http://foo.bar");
        url.openConnection();
        FuelManager fuel = FuelManager.Companion.getInstance();
        Request request = fuel.get(mock.path(""), null);
        HttpURLConnection urlConnection = new HttpURLConnection(url) {
            @Override
            public void connect() throws IOException {
            }

            @Override
            public void disconnect() {
            }

            @Override
            public boolean usingProxy() {
                return false;
            }
        };
        hook.preConnect(urlConnection, request);
    }

    /**
     * stethoLifecycleIsCallingCorrectly
     *
     * @throws IOException IOException
     */
    @Test
    public void stethoLifecycleIsCallingCorrectly() throws IOException {
        ConcurrentHashMap map = hook.getStethoCache();

        FuelManager fuel = FuelManager.Companion.getInstance();
        Request requestGet = fuel.get(mock.path(""), null);
        Request requestPut = fuel.put(mock.path(""), null);
        Request requestDelete = fuel.delete(mock.path(""), null);

        URL url = new URL("http://foo.bar");
        url.openConnection();

        HttpURLConnection urlConnectionGet = new HttpURLConnection(url) {
            @Override
            public void connect() throws IOException {
            }

            @Override
            public void disconnect() {
            }

            @Override
            public boolean usingProxy() {
                return false;
            }
        };

        hook.preConnect(urlConnectionGet, requestGet);
        hook.preConnect(urlConnectionGet, requestPut);
        hook.preConnect(urlConnectionGet, requestDelete);

        hook.postConnect(requestGet);
        hook.postConnect(requestPut);
        hook.postConnect(requestDelete);
        hook.interpretResponseStream(requestDelete, new ByteArrayInputStream(new byte[500]));
        hook.interpretResponseStream(requestPut, new ByteArrayInputStream(new byte[500]));
        hook.interpretResponseStream(requestGet, new ByteArrayInputStream(new byte[500]));
    }
}
